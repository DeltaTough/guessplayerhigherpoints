//
//  PlayerTests.swift
//  GuessPlayerHigherPointsTests
//
//  Created by Dimitrios Tsoumanis on 16/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import XCTest

class PlayerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    func testDecodingStandardType() {
        let json = """
        {
        "first_name": "Stephen",
        "fixture": {
        "_members": [
          "112160"
        ],
        "_ref": "fixtures.id"
        },
        "fppg": 47.94303797468354,
        "id": "15475-9524",
        "images": {
        "default": {
          "height": 200,
          "url": "https://d17odppiik753x.cloudfront.net/playerimages/nba/9524.png",
          "width": 200
        }
        },
        "injured": false,
        "injury_details": "knee",
        "injury_status": "o",
        "last_name": "Curry",
        "news": {
        "latest": "2016-05-02T18:35:15Z"
        },
        "played": 79,
        "player_card_url": "https://www.fanduel.com/e/Player/9524/Stats/15475",
        "position": "PG",
        "removed": false,
        "salary": 10600,
        "starting_order": null,
        "team": {
        "_members": [
          "687"
        ],
        "_ref": "teams.id"
        }
        }
        """.data(using: .utf8)!
        
        let player = try! JSONDecoder().decode(Player.self, from: json)
        
        XCTAssertEqual(player.fullName(), "Stephen Curry")
        XCTAssertEqual(player.fppg, 47.94303797468354)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
