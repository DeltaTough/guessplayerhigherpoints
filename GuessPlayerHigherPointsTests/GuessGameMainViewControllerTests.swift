//
//  GuessGameMainViewControllerTests.swift
//  GuessPlayerHigherPointsTests
//
//  Created by Dimitrios Tsoumanis on 16/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import XCTest
@testable import GuessPlayerHigherPoints

class GuessGameMainViewControllerTests: XCTestCase {
    var mainvc: GuessGameMainViewController!
    
    override func setUp() {
        super.setUp()
        let bundle = Bundle(for: GuessGameMainViewController.self)
        guard let _ = bundle.loadNibNamed("GuessGameMainViewController", owner: nil)?.first as? UIView else {
            return XCTFail("GuessGameMainViewController nib did not contain a UIView")
        }
    }
    
    func testPlayerParser() {
        var fetcher = PlayerFetcher()
        let expectationAssert = expectation(description: "PlayerFetcher returns a players array and runs the callback closure")
        fetcher.fetch { (players) in
            if let players = players {
                XCTAssertNil(players)
            } else {
                XCTFail("Unexpected response")
            }
            expectationAssert.fulfill()
        }
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
