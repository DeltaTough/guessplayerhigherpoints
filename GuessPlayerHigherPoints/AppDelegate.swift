//
//  AppDelegate.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 12/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let initialController = storyboard.instantiateViewController(withIdentifier: "RootView")
        
        self.window?.rootViewController = initialController
        
        self.window!.makeKeyAndVisible()
        
        return true
    }
}

