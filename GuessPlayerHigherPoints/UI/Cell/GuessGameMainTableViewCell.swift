//
//  GuessGameMainCollectionViewCell.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 11/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class GuessGameMainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var playerPointsLabel: UILabel!
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    var hideFullCellDetails: Bool? {
        didSet {
            self.playerPointsLabel.isHidden = self.hideFullCellDetails ?? true
        }
    }
    
    var player: Player? {
        didSet {
            guard let player = self.player else { return }
            self.playerNameLabel.text = player.fullName()
            self.playerPointsLabel.text = String(format: "%.2f", player.fppg ?? 0)
            if let urlString = player.images?.defaultImage.url {
                self.playerImageView.sd_setShowActivityIndicatorView(true)
                self.playerImageView.sd_setIndicatorStyle(.gray)
                self.playerImageView.sd_setImage(with: URL(string: urlString),
                                                 placeholderImage: nil)
            }
            self.imageHeightConstraint.constant = self.playerImageView.contentClippingRect.height
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundedView()
        self.backgroundColor = .black
    }

    // displays view as circle
    fileprivate func roundedView() {
        self.playerImageView.layer.cornerRadius = self.frame.height / 3.6
        self.playerImageView.layer.borderWidth = 1.0
        self.playerImageView.layer.borderColor = UIColor.white.cgColor
        self.playerImageView.clipsToBounds = true
    }
}
