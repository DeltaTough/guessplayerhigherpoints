//
//  GuessGameMainViewController.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 11/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit
import Alamofire
import Toaster

class GuessGameMainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var tableHeaderView: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var indicationLabel: UILabel!
    
    lazy var playerFetcher: PlayerFetcher = {
        let fetcher = PlayerFetcher()
        return fetcher
    }()

    fileprivate var players = [Player]()
    fileprivate var randomPlayer = [Player]()
    
    let scoreConstant = "scoreConstant"
    fileprivate var fppg: Double = 0
    fileprivate var score: Int = 0
    fileprivate var indexPath: IndexPath?
    fileprivate var userWon = false
    fileprivate var hideFullCellDetails = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        UserDefaults.standard.set(self.score, forKey: scoreConstant)
        self.scoreLabel.text = "Score: \(UserDefaults.standard.integer(forKey: scoreConstant))"
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = .black
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.playerFetcher.fetch({ (players) in
            guard let players = players else { return }
            self.players = players
            self.findTwoPlayers()
            self.tableView.reloadOnMainThread()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = .white
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
   fileprivate func setupTableView() {
        self.tableView.register(cell: GuessGameMainTableViewCell.self)
        self.tableView.hideTrailingCellDivider()
        self.tableView.tableHeaderView = self.tableHeaderView
        self.tableHeaderView.backgroundColor = .black
    }
    
    func findTwoPlayers() {
        var randomIndex = 0
        for _ in 0..<2 {
            randomIndex = Int(arc4random_uniform(UInt32(self.players.count)))
            if self.players.indices.contains(randomIndex) {
                self.randomPlayer.append(self.players[randomIndex])
            }
        }
        // we can compare last and first as we always have 2 objects in array
        if self.randomPlayer.first?.id == self.randomPlayer.last?.id {
            self.randomPlayer = [Player]()
            self.findTwoPlayers()
        }
    }
    
    fileprivate func animateImageOnTap(indexPath: IndexPath) {
        // flash the image of player with higher score
        let cell = tableView.cellForRow(at: self.indexPath ?? indexPath)
        UIView.transition(with: self.view,
                          duration: 0.1,
                          options: .curveEaseOut,
                          animations: {
                            (cell as? GuessGameMainTableViewCell)?.playerImageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }) { (completed) in
            (cell as? GuessGameMainTableViewCell)?.playerImageView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.tapCellActions(indexPath: indexPath)
        }
    }
    
    fileprivate func tapCellActions(indexPath: IndexPath) {
        UIView.animate(withDuration: 2.0, animations: {
            self.view.isUserInteractionEnabled = false
            self.userWon = (indexPath == self.indexPath)
            self.hideFullCellDetails = false
            var savedScore = UserDefaults.standard.integer(forKey: self.scoreConstant)
            savedScore = (indexPath == self.indexPath) ? savedScore + 1 : savedScore
            UserDefaults.standard.set(savedScore, forKey: self.scoreConstant)
            self.scoreLabel.text = "Score: \(UserDefaults.standard.integer(forKey: self.scoreConstant))"
            self.indicationLabel.text = (indexPath == self.indexPath) ? "Correct!" : "Wrong!"
            self.tableView.reloadOnMainThread()
        }) { (completed) in
            self.view.isUserInteractionEnabled = true
            self.userWon = false
            self.hideFullCellDetails = true
            self.fppg = 0
            self.indexPath = IndexPath()
            self.randomPlayer = [Player]()
            self.indicationLabel.text = nil
            self.tableHeaderView.backgroundColor = .black
            self.findTwoPlayers()
            self.tableView.reloadOnMainThread()
        }
    }
}

//MARK- TableViewDataSource
extension GuessGameMainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.randomPlayer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard UserDefaults.standard.integer(forKey: scoreConstant) != 10 else {
            Toast(text: "Winner winner, chicken dinner!").show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.navigationController?.popToRootViewController(animated: true)
            }
            return UITableViewCell()
        }
        let cell = tableView.dequeue(cell: GuessGameMainTableViewCell.self, indexPath: indexPath)
        cell.player = self.randomPlayer[indexPath.row]
        cell.hideFullCellDetails = self.hideFullCellDetails
        if self.fppg < self.randomPlayer[indexPath.row].fppg ?? 0 {
            self.fppg = self.randomPlayer[indexPath.row].fppg ?? 0
            self.indexPath = indexPath
        }
        return cell
    }
}

//MARK- TableViewDelegate
extension GuessGameMainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableHeaderView.backgroundColor = (indexPath == self.indexPath) ? .green : .red
        self.animateImageOnTap(indexPath: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let statusBarSize = UIApplication.shared.statusBarFrame.size.height
        let tableHederViewHeight = self.tableHeaderView.frame.size.height
        let cellHeight = (UIScreen.main.bounds.height - (statusBarSize + tableHederViewHeight)) / 2
        return cellHeight
    }
}
