//
//  StartPageViewController.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 12/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit

class StartPageViewController: UIViewController {

    @IBAction func startGame() {
        self.navigationController?.pushViewController(GuessGameMainViewController(), animated: true)
    }
}
