//
//  PlayerFetcher.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 15/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

struct PlayerFetcher {
    lazy var networking: Network = {
       let net = Network()
        return net
    }()
    
    mutating func fetch(_ response: @escaping ([Player]?) -> ()) {
        self.networking.request { (data) in
            do {
                guard let data = data else { return }
                let result = try JSONDecoder().decode(Players.self, from: data)
                guard let players = result.players else { return }
                response(players)
            } catch let error {
                print(error)
            }
        }
    }
}

