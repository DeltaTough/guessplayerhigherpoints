//
//  Network.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 15/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import Alamofire

typealias NetworkCompletion = (Data?) -> ()

struct Network {
    func request(_ networkResponse: @escaping NetworkCompletion) {
        Alamofire.request(Constants.pleyerURL).responseJSON { response in
            guard let data = response.data else { return }
            networkResponse(data)
        }
    }
}


