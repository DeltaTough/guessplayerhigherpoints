//
//  Images.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 13/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

struct Images: Decodable {
    let defaultImage: Default
    
    private enum CodingKeys : String, CodingKey {
        case defaultImage = "default"
    }
}
