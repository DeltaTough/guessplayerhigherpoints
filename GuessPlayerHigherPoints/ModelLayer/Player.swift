//
//  Player.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 11/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

struct Player: Decodable {
    let first_name: String?
    let fppg: Double?
    let id: String?
    let last_name: String?
    let images: Images?
    
    func fullName() -> String? {
       return "\(self.first_name ?? "") \(self.last_name ?? "")"
    }
}
