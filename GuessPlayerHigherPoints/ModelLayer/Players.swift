//
//  Players.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 11/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

struct Players: Decodable {
    let players: [Player]?
}
