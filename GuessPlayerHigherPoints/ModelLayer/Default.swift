//
//  Default.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 13/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

struct Default: Decodable  {
    let height: Double?
    let url: String?
    let width: Double?
}
