//
//  UIView.swift
//  FanDuelGuessGame
//
//  Created by Dimitrios Tsoumanis on 10/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit

extension UIView {
    class func runOnMainThread(block: () -> ())  {
        if Thread.isMainThread {
            block()
        } else {
            DispatchQueue.main.sync {
                block()
            }
        }
    }
    
    static var nibIdentifier: String {
        return self.safeNibName
    }
    
    static var nibForClass: UINib {
        return UINib(nibName: self.safeNibName, bundle: nil)
    }
    
    // return the name of the class without prefixes (split string on `.`)
    static private var safeNibName: String {
        let safeNibName = "\(self)"
        let components = safeNibName.split{$0 == "."}.map ( String.init )
        return components.last ?? ""
    }
}
