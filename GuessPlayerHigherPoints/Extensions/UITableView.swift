//
//  UITableView.swift
//  FanDuelGuessGame
//
//  Created by Dimitrios Tsoumanis on 10/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit

extension UITableView {
    
    func register<T : UITableViewCell>(cell: T.Type) {
        register(cell.nibForClass, forCellReuseIdentifier: cell.nibIdentifier)
    }
    
    func dequeue<T : UITableViewCell>(cell: T.Type, indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: cell.nibIdentifier, for: indexPath) as! T
    }
    
    func reloadOnMainThread() {
        UIView.runOnMainThread { [weak self] in
            self?.reloadData()
        }
    }
    
    func hideTrailingCellDivider() {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 1))
        self.tableFooterView = footerView
    }
}
