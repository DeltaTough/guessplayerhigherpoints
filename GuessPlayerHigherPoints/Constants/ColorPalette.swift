//
//  ColorPalette.swift
//  GuessPlayerHigherPoints
//
//  Created by Dimitrios Tsoumanis on 13/01/2019.
//  Copyright © 2019 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit

struct ColorPalette {
    static let RPGreen = UIColor(red: CGFloat(0), green: CGFloat(104/255.0), blue: CGFloat(55/255.0), alpha: CGFloat(1.0))

    static let RPCyan = UIColor(red: 73/255, green: 166/255, blue: 164/255, alpha: CGFloat(1.0))
}
